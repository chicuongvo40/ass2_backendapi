﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransfer
{
    public class AuthorRequest
    {
        public int AuthorId { get; set; }
        public string EmailAddress { get; set; }
        public string Address { get; set; }
        public string LastName { get; set; }
        public string FistName { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }

    }
}
